# https://www.interviewbit.com/problems/noble-integer/

class Solution:
    # @param A : list of integers
    # @return an integer
    def solve(self, A):
        A.sort()
        len_A = len(A)

        for idx, item in enumerate(A):
            # Skip repeated numbers
            if idx + 1 < len_A and item == A[idx + 1]:
                continue
            if item == len_A - idx - 1:
                return 1

        return -1

